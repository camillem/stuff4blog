# a short script to generate a graph of Kdenlive' finacial situation


import plotly.graph_objects as go

CAMPAIGN_TOTAL = 21816.10
# Donor box fee is 1.5% (+ Payment Processing fee  ? )
DONORBOX_FEE = CAMPAIGN_TOTAL*0.015
NET_TOTAL = CAMPAIGN_TOTAL - DONORBOX_FEE
# Kdenlive gets 80%, KDE eV gets 20%
KDE_EV = NET_TOTAL*0.2
KDENLIVE = NET_TOTAL-KDE_EV

# Referers data are random

MASTODON = CAMPAIGN_TOTAL*0.35
TWITTER = CAMPAIGN_TOTAL*0.15
WEBSITE = CAMPAIGN_TOTAL*0.25
OTHERS = CAMPAIGN_TOTAL*0.25

values = {
    "Mastodon":MASTODON,
    "Twitter":TWITTER,
    "Website":WEBSITE,
    "Other referers":OTHERS,
    "Total amount":CAMPAIGN_TOTAL,
    "Net total":NET_TOTAL,
    "Donor Box fee":DONORBOX_FEE,
    "KDE e.V.":KDE_EV,
    "Kdenlive":KDENLIVE,
}

colors = [
    "rgba(83, 127, 179, 1)",
    "rgba(83, 127, 179, 1)",
    "rgba(83, 127, 179, 1)",
    "rgba(83, 127, 179, 1)",
    "rgba(83, 127, 179, 1)",
    "rgba(83, 127, 179, 1)",
    "rgba(84, 163, 216, 1)",
    "rgba(243, 134, 120, 1)",
]

links = [
    ["Mastodon", "Total amount", MASTODON],
    ["Twitter", "Total amount", TWITTER],
    ["Website", "Total amount", WEBSITE],
    ["Other referers", "Total amount", OTHERS],
    ["Total amount", "Donor Box fee", DONORBOX_FEE],
    ["Total amount", "Net total", NET_TOTAL],
    ["Net total", "KDE e.V.", KDE_EV],
    ["Net total", "Kdenlive", KDENLIVE],
]

relations = dict(source=[], target=[], value=[])
labels = []

for link in links:
    if link[0] not in labels:
        labels.append(link[0])
    if link[1] not in labels:
        labels.append(link[1])
    relations["source"].append(labels.index(link[0]))
    relations["target"].append(labels.index(link[1]))
    relations["value"].append(link[2])

labels = [f"{label} € {int(values[label])}" for label in labels]

print(relations)
fig = go.Figure(
    data=[
        go.Sankey(
            node=dict(
                pad=15,
                thickness=20,
                line=dict(color="black", width=0.5),
                label=labels,
                color=colors,
            ),
            link=relations,
            valuesuffix = "€"
        )
    ]
)

fig.update_layout(title_text="KDEnlive Funding campaign 2022", font_size=10)
fig.show()
fig.write_html("results/funding.html")
fig.write_image("results/funding.png")

