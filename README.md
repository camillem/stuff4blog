# stuff4blog

A repo to create charts for reporting on the funding campaign.

Currently using [Plotly](https://plotly.com/python/sankey-diagram/)


# Licence 

[CC0-1.0](https://spdx.org/licenses/CC0-1.0.html)
